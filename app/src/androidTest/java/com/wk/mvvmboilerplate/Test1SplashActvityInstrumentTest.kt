package com.wk.mvvmboilerplate

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.wk.mvvmboilerplate.ui.splash.SplashActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.utils.PrefUtils
import android.content.Intent
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent

import androidx.test.espresso.intent.rule.IntentsTestRule
import com.wk.mvvmboilerplate.ui.dashboard.DashboardActivity
import com.wk.mvvmboilerplate.ui.login.LoginActivity
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before


@RunWith(AndroidJUnit4::class)
@LargeTest
class Test1SplashActvityInstrumentTest {

    @get:Rule
    val splashActivityRule = IntentsTestRule(SplashActivity::class.java,
        true,
        false)
    lateinit var preferences:PrefUtils


    @Test
    fun navigateToLogin(){
        // Launch activity

        splashActivityRule.launchActivity(Intent())
        IdlingRegistry.getInstance().register(splashActivityRule.activity.idleResource)
        preferences = splashActivityRule.activity.splashViewModel.prefUtils
        preferences.appPref.edit().clear().commit()
        intended(hasComponent(LoginActivity::class.qualifiedName))


    }


    @Test
    fun navigateToDashboard(){

        // Launch activity
        splashActivityRule.launchActivity(Intent())
        IdlingRegistry.getInstance().register(splashActivityRule.activity.idleResource)
        preferences = splashActivityRule.activity.splashViewModel.prefUtils
        var user = User(_id = "123456",
            accessToken = "Bearer token",
            email = "imrank@wekan.company",
            firstName = "imran",lastName = "m",profileImageUrl = "",
            refreshToken = "token",
            status = 1
        )
        preferences.setUserPref(user)

        intended(hasComponent(DashboardActivity::class.qualifiedName))

        preferences.appPref.edit().clear().commit()
    }

    @After
    fun finish(){
        IdlingRegistry.getInstance().unregister(splashActivityRule.activity.idleResource)

    }
}