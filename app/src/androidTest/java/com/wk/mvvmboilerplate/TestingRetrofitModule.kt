package com.wk.mvvmboilerplate

import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.data.remote.ApiInterface
import com.wk.mvvmboilerplate.di.module.NetModule
import com.wk.mvvmboilerplate.di.module.RetrofitModule
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

/**
 * class to provide injection for the object required for network calls
 */
@Module
class TestingRetrofitModule{

    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder().client(okHttpClient).baseUrl("http://localhost:8080/")
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

}