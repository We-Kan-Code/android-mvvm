package com.wk.mvvmboilerplate.data.model.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.wk.mvvmboilerplate.data.model.room.dao.UserDao
import com.wk.mvvmboilerplate.data.model.room.entity.User

@Database(entities = [(User::class)],version = 1)
abstract class AppDatabase :RoomDatabase(){
    abstract fun userDao():UserDao
}