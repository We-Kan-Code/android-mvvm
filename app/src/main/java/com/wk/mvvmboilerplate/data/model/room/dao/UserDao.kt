package com.wk.mvvmboilerplate.data.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wk.mvvmboilerplate.data.model.room.entity.User

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
@Dao
interface UserDao {
    @Query("SELECT * from user")
    fun getAllUser(): List<User>

    @Query("SELECT * from user where email = :n_email")
    fun getUserEmail(vararg n_email:String): List<User>?

    @Query("DELETE from user")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertUser(user: User):Long

    @Query("DELETE from user where _id like :id")
    fun deleteById(id:String)

    @Query("DELETE from user where local_id like :local_id")
    fun deleteByLocalId(local_id:Long)
}

