package com.wk.mvvmboilerplate.data.remote

import android.util.Log
import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.remote.reqres.*
import com.wk.mvvmboilerplate.utils.PrefUtils
import javax.inject.Inject

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class DashboardDataSource @Inject constructor(private val apiInterface: ApiInterface,private val prefUtils: PrefUtils) {

    @Inject
    lateinit var moshi:Moshi

    fun loadUser(): Result<UserData> {
            try {
                val user = prefUtils.getPrefUser()
                val response = apiInterface.userDetailsApi(user?.accessToken!!,user?._id!!,"email,firstName,lastName,profileImageUrl,status").execute()
                Log.i("loggedInUser ", response.toString())
                return when {
                    response.body() is UserDetailsReponse -> {
                        val userData = response.body()?.data
                        Log.i("userData body", userData.toString())
                        Result.Success(userData!!)
                    }
                    response.errorBody()!=null -> {
                        var adapter = moshi.adapter(BaseResponse::class.java).lenient()
                        var error = adapter.fromJson(response.errorBody()?.string()!!)
                        Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                    }
                    else -> Result.Error(resId = R.string.unknown_error)
                }
            } catch (e: Throwable) {
                e.printStackTrace()
                return Result.Error(resId = R.string.unknown_error)
            }
    }

    fun loadAllUsers(): Result<UserList> {
        try {
            val user = prefUtils.getPrefUser()
            val response = apiInterface.allUserDetailsApi(user?.accessToken!!,"email,firstName,lastName,profileImageUrl,status","firstName|asc").execute()
            Log.i("loggedInUser ", response.toString())
            return when {
                response.body() is AllUserDetailsReponse -> {
                    val userData = response.body()?.data
                    Log.i("userData body", userData.toString())
                    Result.Success(userData!!)
                }
                response.errorBody()!=null -> {
                    var adapter = moshi.adapter(BaseResponse::class.java).lenient()
                    var error = adapter.fromJson(response.errorBody()?.string()!!)
                    Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                }
                else -> Result.Error(resId = R.string.unknown_error)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return Result.Error(resId = R.string.unknown_error)
        }
    }
}