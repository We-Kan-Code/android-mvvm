package com.wk.mvvmboilerplate.data.remote

import android.util.Log
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.room.dao.UserDao
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.data.remote.reqres.UserData
import com.wk.mvvmboilerplate.data.remote.reqres.UserList
import javax.inject.Inject

/**
 * Repository Class that handles api call or db access.
 */

class DashboardRepository @Inject constructor(private val dashboardDataSource:DashboardDataSource,private val userDao: UserDao) {


    /**
     * function to user by id from server
     */
    fun loadUser():Result<UserData>?{
        return dashboardDataSource.loadUser()
    }

    /**
     * function to load all users from server
     */
    fun loadAllUsers():Result<UserList>?{
        return dashboardDataSource.loadAllUsers()
    }
    /**
     * function to get users from Room db
     */
    fun getAllUsersfromDB():Result<List<User>>?{
//         insert to user table using the below code
        var userList = userDao.getAllUser()
        Log.i("userlist","${userList.size}")
        return if(userList?.size > 0)
            Result.Success(userList)
        else Result.Error(statusCode = R.string.unknown_error)
    }
}