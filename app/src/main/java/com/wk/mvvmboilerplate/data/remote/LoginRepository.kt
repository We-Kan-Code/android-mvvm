package com.wk.mvvmboilerplate.data.remote

import android.util.Log
import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.remote.reqres.BaseResponse
import com.wk.mvvmboilerplate.data.remote.reqres.LoginReponse
import com.wk.mvvmboilerplate.data.remote.reqres.LoginRequest
import com.wk.mvvmboilerplate.data.remote.reqres.UserData
import com.wk.mvvmboilerplate.utils.PrefUtils
import javax.inject.Inject

/**
 * Repository Class that handles user login.
 */

class LoginRepository @Inject constructor(val apiInterface: ApiInterface, val prefUtil: PrefUtils) {

    @Inject
    lateinit var moshi: Moshi

    /**
     * function for login user
     */
    fun login(username: String, password: String): Result<UserData>? {

        try {
            // TODO: handle loggedInUser authentication
            val response = apiInterface.loginApi(LoginRequest(username, password)).execute()
            Log.i("loggedInUser ", response.toString())
            return when {
                response.body() is LoginReponse -> {
                    val userData = response.body()?.data
//                    Log.i("userData body", userData.toString())
                    //val userData =(result as Result.Success<UserData>).data
                    var authbearer = "Bearer ${userData?.user?.accessToken}"
                    userData?.user?.accessToken = authbearer
                    prefUtil.setUserPref(userData?.user!!)
                    print("success")
                    Result.Success(userData!!)
                }
                response.errorBody()!=null -> {
                    var adapter = moshi.adapter(BaseResponse::class.java).lenient()
                    var error = adapter.fromJson(response.errorBody()?.string()!!)
                    print("Error")
                    Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                }
                else -> {
                    print("Error unknown")
                    Result.Error(resId = R.string.unknown_error)}
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return Result.Error(resId = R.string.unknown_error)
        }
    }

}
