package com.wk.mvvmboilerplate.data.remote

import android.util.Log
import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.data.remote.reqres.*
import com.wk.mvvmboilerplate.utils.PrefUtils
import javax.inject.Inject

/**
 * Repository Class that handles user sign up.
 */

class SignupRepository @Inject constructor(private val apiInterface: ApiInterface, private val prefUtil: PrefUtils) {

    @Inject
    lateinit var moshi:Moshi
    /**
     * function for sign up user
     */
    fun signUp(email: String, firstName: String,lastName: String, password: String, profileImageUrl: String): Result<User> {
        try {
            val response = apiInterface.signupApi(SignupRequest(email = email,firstName = firstName,lastName = lastName, password = password,profileImageUrl = profileImageUrl)).execute()
            Log.i("loggedInUser ", response .toString())
//            Log.i("loggedInUser body", response.errorBody()?.string())
//            val fakeUser = LoggedInUser(java.util.UUID.randomUUID().toString(), username,username,"atoken","rtoken")
            return when {
                response.body() is SignupResponse -> {
                    val userData = response.body()?.data?.user
                    prefUtil.setUserPref(userData!!)
                    Result.Success(userData!!)
                }
                response.errorBody()!=null -> {

//                    Log.i("loggedInUser body", response.errorBody()?.string())
                    var adapter = moshi.adapter(BaseResponse::class.java).lenient()
                    var error = adapter.fromJson(response.errorBody()?.string()!!)
                    Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                }
                else -> Result.Error(resId = R.string.unknown_error)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return Result.Error(resId = R.string.unknown_error)
        }
    }
}
