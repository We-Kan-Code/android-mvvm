package com.wk.mvvmboilerplate.data.remote

import android.util.Log
import com.wk.mvvmboilerplate.data.remote.reqres.RefreshTokenResponse
import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.BuildConfig
import com.wk.mvvmboilerplate.data.remote.reqres.RefreshTokenRequest
import com.wk.mvvmboilerplate.utils.PrefUtils
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.io.IOException
import javax.inject.Inject

/**
 * Authenticator Class that handles auth token expiry.
 */
class TokenAuthenticator @Inject constructor(val preferenceHelper: PrefUtils,val moshi:Moshi): Authenticator {
    @Throws(IOException::class)
    override fun authenticate(route: Route?, responset: Response): Request? {
        try {
            var user = preferenceHelper.getPrefUser()
            var oldToken = user?.refreshToken
            var requestAuthToken = responset.header("Authorization","")!!
            Log.i("TokenAuthenticator", "${requestAuthToken}")
            if (responset.code == 401 && requestAuthToken.isNotEmpty() && oldToken!=null && oldToken.isNotEmpty()) {
                Log.i("TokenAuthenticator", "${responset.code}")


                var requestParam = RefreshTokenRequest(oldToken!!)
                val adapter = moshi.adapter(RefreshTokenRequest::class.java).lenient()
                        val reqstr = adapter.toJson(requestParam)
                val JSON = "application/json; charset=utf-8".toMediaTypeOrNull()
                val reqbody = RequestBody.create(JSON,reqstr)
                val request = Request.Builder()
                    .url(BuildConfig
                        .BASE_URL + "auth/users/refresh")
                    .method("POST", reqbody)
                    .build()
                val client = OkHttpClient()

                client.newCall(request).execute().use { response ->
                    if (!response.isSuccessful) throw IOException("Unexpected code $response")
                    val json = response.body!!.string()
                    //println(json)
                    val adapter = moshi.adapter(RefreshTokenResponse::class.java).lenient()
                    val responseObj = adapter.fromJson(json)
                    if (responseObj is RefreshTokenResponse) {
                        user =responseObj.data.user
                        user?.accessToken = "Bearer ${responseObj.data.user.accessToken}"
                        preferenceHelper.setUserPref(user = user!!)
                    }
                }
                val newAuthToken = user?.accessToken
        //             AuthRefreshHelper().callAuthRefresher(rxCallApi,preferences)

                Log.i("TokenAuthenticator", "${newAuthToken}")
                if (newAuthToken != null && !oldToken.equals(newAuthToken)) {
                    return responset.request.newBuilder()
                            .header("Authorization", newAuthToken)
                            .build()
                }
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
        return null
    }
}