package com.wk.mvvmboilerplate.data.remote

import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.data.remote.reqres.BaseResponse
import com.wk.mvvmboilerplate.data.remote.reqres.SignupResponse
import com.wk.mvvmboilerplate.data.remote.reqres.VerifyOTPRequest
import com.wk.mvvmboilerplate.utils.PrefUtils
import javax.inject.Inject

import android.util.Log

/**
 * Repository Class that handles otp verification of user and resend otp .
 */

class VerifyOTPRepository @Inject constructor(private val apiInterface: ApiInterface) {

    @Inject
    lateinit var moshi: Moshi
    @Inject
    lateinit var prefUtil: PrefUtils

    /**
     * function for verifying otp
     */
    fun verifyOTP(useId: String, code: String): Result<User> {
        try {
            val response = apiInterface.verifyOTPApi(useId,
                VerifyOTPRequest(verificationCode= code)
            ).execute()
            Log.i("loggedInUser ", response .toString())
            return when {
                response.body() is SignupResponse -> {
                    val userData = response.body()?.data?.user
                    Log.i("userData body", userData.toString())
                    prefUtil.setUserPref(userData!!)
                    Result.Success(userData!!)
                }
                response.errorBody()!=null -> {
                    var adapter = moshi.adapter(BaseResponse::class.java).lenient()
                    var error = adapter.fromJson(response.errorBody()?.string()!!)
                    Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                }
                else -> Result.Error(resId = R.string.unknown_error)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return Result.Error(resId = R.string.unknown_error)
        }
    }
    /**
     * function for resend otp
     */
    fun resendOTP(useId: String): Result<String> {
        try {
            val response = apiInterface.resendOTPApi(useId).execute()
            return when {
                response.code() == 204 -> {
                    Result.Success("verification code send to your mail")
                }
                response.errorBody()!=null -> {
                    var adapter = moshi.adapter(BaseResponse::class.java).lenient()
                    var error = adapter.fromJson(response.errorBody()?.string()!!)
                    Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                }
                else -> Result.Error(resId = R.string.unknown_error)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return Result.Error(resId = R.string.unknown_error)
        }
    }
}


