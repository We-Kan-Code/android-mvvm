package com.wk.mvvmboilerplate.data.remote.reqres

import com.squareup.moshi.Json
import com.wk.mvvmboilerplate.data.model.room.entity.User

data class LoginReponse(
    @field:Json(name = "data")
    val `data`: UserData
):BaseResponse()
data class UserData(

    @Json(name = "user")
    val user: User
)
data class UserList(
    @Json(name = "users")
    val user: List<User>
)