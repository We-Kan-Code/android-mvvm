package com.wk.mvvmboilerplate.data.remote.reqres

import com.squareup.moshi.Json

data class RefreshTokenRequest(
    @Json(name = "refreshToken")
    val refreshToken:String?=null
)