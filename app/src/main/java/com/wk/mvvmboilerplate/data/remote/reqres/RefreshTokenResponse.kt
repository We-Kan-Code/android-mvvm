package com.wk.mvvmboilerplate.data.remote.reqres

import com.squareup.moshi.Json
import com.wk.mvvmboilerplate.data.remote.reqres.BaseResponse
import com.wk.mvvmboilerplate.data.remote.reqres.UserData

/**
 * Created by WEKANCODE on 11/01/18.
 */
class RefreshTokenResponse(
        @field:Json(name = "data")
        val `data`: UserData
): BaseResponse()


