package com.wk.mvvmboilerplate.data.remote.reqres


import com.squareup.moshi.Json
import com.wk.mvvmboilerplate.data.model.room.entity.User

data class SignupResponse(
    @Json(name = "data")
    val `data`: SignupData
):BaseResponse()
data class SignupData(
    @Json(name = "user")
    val user: User
)