package com.wk.mvvmboilerplate.data.remote.reqres


import com.squareup.moshi.Json

data class  UpdateProfileRequest constructor(
    @Json(name = "firstName")
    val firstName: String,
    @Json(name = "lastName")
    val lastName: String
)
data class UpdatePasswordRequest(
    @Json(name = "password")
    val password: String
)
data class UpdateProfilePictureRequest(
    @Json(name = "profileImageUrl")
    val profileImageUrl: String
)