package com.wk.mvvmboilerplate.data.remote.reqres

import com.squareup.moshi.Json

data class UserDetailsReponse(
    @field:Json(name = "data")
    val `data`: UserData
):BaseResponse()

data class AllUserDetailsReponse(
    @field:Json(name = "data")
    val `data`: UserList
):BaseResponse()