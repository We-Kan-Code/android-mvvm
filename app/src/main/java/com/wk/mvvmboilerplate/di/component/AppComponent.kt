package com.wk.mvvmboilerplate.di.component

import android.app.Application
import androidx.work.Worker
import com.wk.mvvmboilerplate.App
import com.wk.mvvmboilerplate.di.module.*
import com.wk.mvvmboilerplate.service.FCMService
import com.wk.mvvmboilerplate.work.SyncWorker
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * class to define the components that are provided for the app with the defined list of modules
 */
@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),(AppModule::class),(NetModule::class),(RetrofitModule::class),(ActivityBuldersModule::class),(ServiceModule::class)]
)
interface AppComponent {
    fun inject(app: App)
    fun inject(worker: SyncWorker)
}