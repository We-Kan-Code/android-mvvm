package com.wk.mvvmboilerplate.di.module

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.wk.mvvmboilerplate.BuildConfig
import com.wk.mvvmboilerplate.data.remote.ApiInterface
import com.wk.mvvmboilerplate.data.remote.TokenAuthenticator
import com.wk.mvvmboilerplate.utils.PrefUtils
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import javax.inject.Singleton

/**
 * class to provide injection for the object required for network calls
 */
@Module
open class NetModule{
    @Provides
    @Singleton
    fun providesMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
    @Provides
    @Singleton
    fun provideTokenAuthenticator(pref:PrefUtils,moshi:Moshi) = TokenAuthenticator(preferenceHelper = pref,moshi = moshi)
    @Provides
    @Singleton
    fun providesAuthOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor,tokenAuthenticator:TokenAuthenticator): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .authenticator(tokenAuthenticator)
            .build()
    }

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Timber.tag("OkHttp").i(message)
            }
        })
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides
    @Singleton
    fun providesApiInterface(retrofit: Retrofit): ApiInterface = retrofit.create(
        ApiInterface::class.java)

}