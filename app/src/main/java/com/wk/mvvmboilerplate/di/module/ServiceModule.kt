package com.wk.mvvmboilerplate.di.module

import com.wk.mvvmboilerplate.service.FCMService
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ServiceModule {

    @ContributesAndroidInjector
    abstract fun contributeMyService(): FCMService
}