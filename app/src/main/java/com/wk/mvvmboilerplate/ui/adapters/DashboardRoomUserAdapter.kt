package com.wk.mvvmboilerplate.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.realm.Users
import com.wk.mvvmboilerplate.data.model.room.entity.User
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlin.coroutines.coroutineContext

class DashboardRoomUserAdapter(val data:List<User>, val context:Context) : RecyclerView.Adapter<DashboardRoomUserAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return data.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_row_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = data!![position]
        holder.name.text = "${user.firstName } ${user.lastName }"
        holder.active.text = context.resources.getStringArray(R.array.active)[user.status]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var name:TextView = itemView.findViewById(R.id.name)
        var active:TextView = itemView.findViewById(R.id.active)
    }
}