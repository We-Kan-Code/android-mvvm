package com.wk.mvvmboilerplate.ui.addnewuser

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.remote.SignupRepository
import com.wk.mvvmboilerplate.ui.login.BaseViewModel
import com.wk.mvvmboilerplate.data.remote.Result
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.isEmailValid
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import android.util.Log
import com.facebook.FacebookSdk.getApplicationContext
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.data.remote.AddNewUserRepository
import com.wk.mvvmboilerplate.ui.signup.SignupFormState
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.convertToHashString
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.validatePassword
import java.io.File
import java.time.Instant
import java.time.LocalDateTime
import java.util.*

/**
 * View model class for sign up
 */
class AddNewUserViewModel @Inject constructor(private val addNewUserRepository: AddNewUserRepository): BaseViewModel() {
    /**
     * livedata to observe sign up form data change
     */
    private val _addNewUserForm = MutableLiveData<AddNewUserFormState>()
    val addNewUserFormState: LiveData<AddNewUserFormState> = _addNewUserForm
    /**
     * livedata to observe sign up api result data
     */
    private val _addNewUserResult = MutableLiveData<AddNewUserResult>()
    val addNewUserResult: LiveData<AddNewUserResult> = _addNewUserResult
    /**
     * function for sign up api call
     */
    fun addNewUser(email: String, firstName: String,lastName: String){
        // can be launched in a separate asynchronous job
        launch {
            var newuser = User(local_id = Instant.now().toEpochMilli(),email = email,firstName = firstName,lastName = lastName)
            val result = addNewUserRepository.insertUser(newuser)
            withContext(Dispatchers.Main){
                if (result>-1)
                    _addNewUserResult.value = AddNewUserResult(success = newuser)
                else
                    _addNewUserResult.value = AddNewUserResult(errorCode = R.string.user_exist)
            }

            //Log.i("result",result.toString())
        }
    }
    /**
     * function for validating sign up form data change
     */
    fun addNewUserDataChanged(username: String,firstName: String,lastName: String) {
        if (!isEmailValid(username)) {
            _addNewUserForm.value = AddNewUserFormState(emailError = R.string.invalid_email)
        } else if (firstName.isEmpty()){
            _addNewUserForm.value = AddNewUserFormState(firstNameError = com.wk.mvvmboilerplate.R.string.no_firstname)
        } else if (lastName.isEmpty()){
            _addNewUserForm.value = AddNewUserFormState(lastNameError = com.wk.mvvmboilerplate.R.string.no_lastname)
        } else {
            _addNewUserForm.value = AddNewUserFormState(isDataValid = true)
        }
    }

}