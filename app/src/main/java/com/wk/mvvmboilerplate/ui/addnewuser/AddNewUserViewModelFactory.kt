package com.wk.mvvmboilerplate.ui.addnewuser

import androidx.lifecycle.ViewModel
import com.wk.mvvmboilerplate.ui.login.BaseViewModelFactory
import javax.inject.Inject

class AddNewUserViewModelFactory @Inject constructor(private val signupViewModel:AddNewUserViewModel):BaseViewModelFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddNewUserViewModel::class.java)) {
            return signupViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}