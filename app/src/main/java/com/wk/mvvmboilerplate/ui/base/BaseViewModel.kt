package com.wk.mvvmboilerplate.ui.login

import android.util.Log
import androidx.lifecycle.ViewModel
import com.wk.mvvmboilerplate.BuildConfig
import com.wk.mvvmboilerplate.utils.PrefUtils
import io.realm.*

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext
import javax.inject.Inject


/**
 * Base view model for common functionality
 */
open class BaseViewModel : ViewModel(),CoroutineScope {
    private var coroutineJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = coroutineJob + Dispatchers.IO

    var realm: Realm? = null

    @Inject
    lateinit var prefUtils: PrefUtils
    /**
     * function authenticate realm sync user for cloud db
     */
    fun authRealmUser(){
        val credentials = SyncCredentials.usernamePassword(BuildConfig.REALM_USER, BuildConfig.REALM_PASS, false)
//        SyncUser.logInAsync(credentials, BuildConfig.REALM_AUTH_URL,object : SyncUser.Callback<SyncUser>{
//            override fun onSuccess(result: SyncUser) {
//                prefUtils.setRealmSyncUser(result.toJson())
//            }
//
//            override fun onError(error: ObjectServerError) {
//                Log.e("error",error.exception?.message)
//            }
//
//        })
        var realmUser = SyncUser.logIn(credentials, BuildConfig.REALM_AUTH_URL)
        prefUtils.setRealmSyncUser(realmUser .toJson())
    }

    /**
     * function to initialize realm with realm sync user
     */
    fun initializeSyncRealm(){
        var syncConfig = SyncUser.fromJson(prefUtils.getRealmSyncUser())
            .createConfiguration(BuildConfig.REALM_DB_URL)
            .fullSynchronization()
            .build()
        realm = Realm.getInstance(syncConfig)
        realm?.isAutoRefresh = true
    }

    /**
     * function to initialize realm with default config
     */
    fun initializeRealm(){

        var config = RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .build()
//        var config = Realm.getDefaultConfiguration()
//        config?.shouldDeleteRealmIfMigrationNeeded()
        realm = Realm.getInstance(config)

    }

    override fun onCleared() {
        super.onCleared()
        coroutineJob.cancel()
        realm?.close()
    }
}
