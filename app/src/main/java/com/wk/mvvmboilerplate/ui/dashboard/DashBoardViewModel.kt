package com.wk.mvvmboilerplate.ui.dashboard

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.idling.CountingIdlingResource
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.realm.Users
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.data.remote.DashboardRepository
import com.wk.mvvmboilerplate.data.remote.Result
import com.wk.mvvmboilerplate.data.remote.reqres.UserList
import com.wk.mvvmboilerplate.ui.login.BaseViewModel
import com.wk.mvvmboilerplate.utils.PrefUtils
import io.reactivex.Observable
import io.realm.RealmChangeListener
import io.realm.RealmResults
import io.realm.SyncUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject
import kotlin.math.log

    // For Realm uncomment this
/**
 * View model class for dashboard
 */
class DashBoardViewModel @Inject constructor(private val dashboardRepository: DashboardRepository ): BaseViewModel() {
    var idleResource = CountingIdlingResource("dash")
    /**
     * livedata to observe load user api result data
     */
    private val _dashboardUserDetailsApi = MutableLiveData<DashboardResult>()
    val dashboardUserDetailsApiResult: LiveData<DashboardResult> = _dashboardUserDetailsApi
    private val _dashboardApi = MutableLiveData<DashboardResult>()
    private val _dashboardDB = MutableLiveData<DashboardResult>()
    private val _dashboard = MediatorLiveData<DashboardResult>()
    val dashboardDBResult: LiveData<DashboardResult> = _dashboardDB
    val dashboardApiResult: LiveData<DashboardResult> = _dashboardApi
    val dashboardResult: LiveData<DashboardResult> = _dashboard

    // For Realm uncomment this
//    init {
//        initializeRealm()
//        listenLoadUserDataFromRealm()
//    }

    // For Realm uncomment this
//    private fun listenLoadUserDataFromRealm(){
//        var userResult = realm?.where(Users::class.java)?.findAllAsync()
//        userResult?.addChangeListener(RealmChangeListener<RealmResults<Users>>{
////            Log.i("it.size","${it.size}")
////            var userList = mutableListOf<User>()
////            it.forEach{
////                var user = User()
////                user.apply { _id = it.id
////                    firstName = it.firstName
////                    lastName = it.lastName
////                    email = it.email
////                    profileImageUrl = it.profileImageUrl
////                    status = it.status}
////                userList.add(user)
////            }
//            if(it.isValid)
//            _dashboard.value = DashboardResult(userList = it)
//        })
//    }

    fun loadAllUsers(){
        launch{
            idleResource.increment()
            val apiData = loadAllUserData()
            val dbData = loadAllDBUserData()

            withContext(Dispatchers.Main){

                _dashboard.removeSource(_dashboardDB)
                _dashboard.removeSource(_dashboardApi)

                _dashboardApi.value = apiData
                _dashboardDB.value = dbData
                _dashboard.addSource(_dashboardDB) {
                    _dashboard.value = combineLatestData()
                }
                _dashboard.addSource(_dashboardApi) {
                    _dashboard.value = combineLatestData()
                }
                idleResource.decrement()
            }

        }
    }
    var userList = mutableListOf<User>()
    private fun combineLatestData(
    ): DashboardResult {

        val apiList = dashboardApiResult.value
        val dbList = dashboardDBResult.value

        if (apiList?.userListRoom != null && !userList.containsAll(apiList.userListRoom)) {
            userList.addAll(apiList.userListRoom)
        }


        if (dbList?.userListRoom != null && !userList.containsAll(dbList.userListRoom)){
            userList.addAll(dbList.userListRoom)
        }

        var uniquesSet = userList.toSet()
        userList.clear()
        userList.addAll(uniquesSet.toList())
        return DashboardResult(userListRoom = userList)
    }

    /**
     * function to load user data form db
     */
    private fun loadAllDBUserData():DashboardResult?{
        var result = dashboardRepository.getAllUsersfromDB()
        return when (result) {
            is Result.Success -> {
                var loginUser = result
                DashboardResult(userListRoom = loginUser.data!!)
            }
            is Result.Error -> {
                DashboardResult(error = result.msg)
            }
            else -> {
                DashboardResult(errorCode = R.string.unknown_error)
            }
        }

    }

    /**
     * function to load user data
     */
    private fun loadAllUserData():DashboardResult?{
        var result = dashboardRepository.loadAllUsers()
        return when (result) {
            is Result.Success -> {
                var loginUser = result.data.user
                DashboardResult(userListRoom = loginUser)
            }
            is Result.Error -> {
                if(result.statusCode == 401){
                    logoutUser()
                }
                DashboardResult(error = result.msg)
            }
            else -> {
                DashboardResult(errorCode = R.string.unknown_error)
            }
        }
    }

    // For Realm uncomment this
//    /**
//     * function to load user data
//     */
//    fun loadAllUserDataToRealm(){
//        idleResource.increment()
//        launch {
//        var result = dashboardRepository.loadAllUsers()
//            withContext(Dispatchers.Main) {
//        when (result) {
//            is Result.Success -> {
//                var loginUser = result.data.user
////                        add user details to realm db
//                        addUserDetailToRealm(loginUser)
////                DashboardResult(userListRoom = loginUser)
//            }
//            is Result.Error -> {
//
//                if(result.statusCode == 401){
//                    logoutUser()
//                }
//                _dashboard.value = DashboardResult(error = result.msg)
//            }
//            else -> {
//                _dashboard.value = DashboardResult(errorCode = R.string.unknown_error)
//            }
//        }
//        idleResource.decrement()
//            }
//        }
//
//    }

    // For Realm uncomment this
    /**
     * function to load user data
     */
    fun loadUserData(){
        idleResource.increment()
        launch {
            var result = dashboardRepository.loadUser()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success -> {
                        var loginUser = result.data.user
//                        add user details to realm db
//                        addUserDetailToRealm(loginUser)
                        _dashboardUserDetailsApi.value = DashboardResult(user = loginUser)
                    }
                    is Result.Error -> {
                        _dashboardUserDetailsApi.value = DashboardResult(error = result.msg)
                        if(result.statusCode == 401){
                            logoutUser()
                        }
                    }
                    else -> {
                        _dashboardUserDetailsApi.value = DashboardResult(errorCode = R.string.unknown_error)
                    }
                }
            }
            idleResource.decrement()
        }

    }
// For Realm uncomment this
    /**
     * function to load user data
     */
    fun loadPrefUserData() {
        idleResource.increment()
        var loginUser = prefUtils.getPrefUser()
        if (loginUser != null) {
            _dashboardUserDetailsApi.value = DashboardResult(user = loginUser)
        } else {
            logoutUser()
        }
        idleResource.decrement()
    }
//    /**
//     * function add user details to realm db
//     */
//    private fun addUserDetailToRealm(loginUser:User){
//        realm?.executeTransaction {
//            var user = Users()
//            user.id = loginUser._id
//            user.firstName = loginUser.firstName
//            user.lastName = loginUser.lastName
//            user.email = loginUser.email
//            user.profileImageUrl = loginUser.profileImageUrl
//            user.status = loginUser.status
//            it.insertOrUpdate(user)
//        }
//    }
//
//    /**
//     * function add all users details to realm db
//     */
//    private fun addUserDetailToRealm(users:List<User>){
//        var userList = mutableListOf<Users>()
//        users.forEach{
//            var user = Users()
//                user.apply {id = it._id
//                    firstName = it.firstName
//                    lastName = it.lastName
//                    email = it.email
//                    profileImageUrl = it.profileImageUrl
//                    status = it.status}
//                userList.add(user)
//        }
//        realm?.executeTransaction {
//            it.insertOrUpdate(userList)
//        }
//    }

    /**
     * function to logout user
     */
    fun logoutUser(){
        launch {
            val pref = prefUtils.appPref.edit()
            pref.clear()
            pref.commit()
            withContext(Dispatchers.Main){
                realm?.executeTransaction {
                    realm?.deleteAll()
                }
                _dashboard.value = DashboardResult(user = null,logout = true)
            }
        }

    }
}