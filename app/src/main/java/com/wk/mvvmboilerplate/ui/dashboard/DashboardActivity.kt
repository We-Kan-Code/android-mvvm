package com.wk.mvvmboilerplate.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.ui.adapters.DashboardUsersAdapter
import com.wk.mvvmboilerplate.ui.base.BaseActivity
import com.wk.mvvmboilerplate.ui.login.LoginActivity
import com.wk.mvvmboilerplate.ui.updateprofile.UpdateProfileActivity
import com.wk.mvvmboilerplate.utils.Constants
import dagger.android.AndroidInjection

import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import javax.inject.Inject
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wk.mvvmboilerplate.BuildConfig
import com.wk.mvvmboilerplate.data.remote.AddNewUserRepository
import com.wk.mvvmboilerplate.ui.adapters.DashboardRoomUserAdapter
import com.wk.mvvmboilerplate.ui.addnewuser.AddNewUserActivity


/**
 * Activity for dashboard or landing page after successful login
 */
class DashboardActivity : BaseActivity() {
    //initializing dashboard view model factory
    @Inject
    lateinit var dashboardViewModelFactory:DashboardViewModelFactory
    lateinit var dashBoardViewModel: DashBoardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(toolbar)

        AndroidInjection.inject(this)

        //initializing dashboard view model
        dashBoardViewModel = ViewModelProviders.of(this,dashboardViewModelFactory).get(DashBoardViewModel::class.java)

        //observing the viewmodel for dashboard api result change
        dashBoardViewModel.dashboardResult.observe(this@DashboardActivity, Observer{
            val dashResult = it ?: return@Observer
            if (dashResult.logout) {
                navToLogin()
            }
            if (dashResult.error != null) {
                showToast(dashResult.error)
            }
            if (dashResult.errorCode != null) {
                showToast(dashResult.errorCode)
            }
            if (dashResult.userList != null) {
                var usersAdapter = DashboardUsersAdapter(dashResult.userList,this)
                user_list.adapter = usersAdapter
                user_list.setLayoutManager(LinearLayoutManager(this))
                user_list.setHasFixedSize(true)
                user_list.addItemDecoration(
                    DividerItemDecoration(
                        this,
                        DividerItemDecoration.VERTICAL
                    )
                )

            }
            if (dashResult.userListRoom != null) {
                var usersAdapter = DashboardRoomUserAdapter(dashResult.userListRoom,this)
                user_list.adapter = usersAdapter
                user_list.setLayoutManager(LinearLayoutManager(this))
                user_list.setHasFixedSize(true)
                user_list.addItemDecoration(
                    DividerItemDecoration(
                        this,
                        DividerItemDecoration.VERTICAL
                    )
                )

            }

        })

        dashBoardViewModel.dashboardUserDetailsApiResult.observe(this@DashboardActivity, Observer{
            val dashResult = it ?: return@Observer
            if (dashResult.logout) {
                navToLogin()
            }
            if (dashResult.error != null) {
                showToast(dashResult.error)
            }
            if (dashResult.errorCode != null) {
                showToast(dashResult.errorCode)
            }
            if (dashResult.user != null) {
                user_name.text = "${dashResult.user.firstName} ${dashResult.user.lastName}"
                email.text = dashResult.user.email
            }
        })
    }


    override fun onResume() {
        super.onResume()
        dashBoardViewModel.loadPrefUserData()
        // For API && Room use below
        dashBoardViewModel.loadAllUsers()

        // For API && Realm use below
        // dashBoardViewModel.loadAllUserDataToRealm()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(com.wk.mvvmboilerplate.R.menu.menu_dashboard,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_update_profile-> {
            // User chose the "User Profile update" item, show the app Profile update UI...
            navToUpdateProfile(true)
            true
        }
        R.id.action_change_pass-> {
            // User chose the "Change password" item, show the app Change password UI...
            navToUpdateProfile(false)
            true
        }
        R.id.action_logout -> {
            // User chose the "logout" action, logout current user of app
            dashBoardViewModel.logoutUser()
            true
        }R.id.action_add_new_user -> {
            // User chose the "add new user" action, show add new user UI
            navToAddNewUser()
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    /**
     * Navigate to Update profile Activity and open update profile or change password deppending on the boolean sent in UPDATE_CHANGEPASS.
     */
    private fun navToUpdateProfile(boolean: Boolean){
        var intent = Intent(this@DashboardActivity,UpdateProfileActivity::class.java)
        intent.apply { putExtra(Constants.UPDATE_CHANGEPASS,boolean) }
        startActivity(intent)
    }
    /**
     * Navigate to Add New User Activity
     */
    private fun navToAddNewUser(){
        var intent = Intent(this@DashboardActivity,AddNewUserActivity::class.java)
        startActivity(intent)
    }

    /**
     * Navigate to Login Activity.
     */
    private fun navToLogin(){
        startActivity(Intent(this@DashboardActivity,LoginActivity::class.java))
        finish()
    }
}
