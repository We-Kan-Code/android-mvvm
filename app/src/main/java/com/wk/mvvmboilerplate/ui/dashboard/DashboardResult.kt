package com.wk.mvvmboilerplate.ui.dashboard

import com.wk.mvvmboilerplate.data.model.realm.Users
import com.wk.mvvmboilerplate.data.model.room.entity.User
import io.realm.RealmResults

/**
 * Data validation for load user Api of the dashboard.
 */
data class DashboardResult(
    val user: User? = null,
    val userList: RealmResults<Users>? = null,
    val userListRoom: List<User>? = null,
    val errorCode: Int? = null,
    val error: String? = null,
    val logout: Boolean = false
)
