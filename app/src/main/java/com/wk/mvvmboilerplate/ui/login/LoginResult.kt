package com.wk.mvvmboilerplate.ui.login

import com.wk.mvvmboilerplate.data.model.room.entity.User

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
    val success: User? = null,
    val errorCode: Int? = null,
    val error: String? = null
)
