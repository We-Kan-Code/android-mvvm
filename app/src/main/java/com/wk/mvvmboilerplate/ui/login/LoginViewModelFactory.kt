package com.wk.mvvmboilerplate.ui.login

import androidx.lifecycle.ViewModel
import javax.inject.Inject

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory @Inject constructor(private val loginViewModel: LoginViewModel): BaseViewModelFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return loginViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
