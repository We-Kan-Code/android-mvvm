package com.wk.mvvmboilerplate.ui.signup

import androidx.lifecycle.ViewModel
import com.wk.mvvmboilerplate.ui.login.BaseViewModelFactory
import javax.inject.Inject

class SignupViewModelFactory @Inject constructor(private val signupViewModel:SignupViewModel):BaseViewModelFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignupViewModel::class.java)) {
            return signupViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}