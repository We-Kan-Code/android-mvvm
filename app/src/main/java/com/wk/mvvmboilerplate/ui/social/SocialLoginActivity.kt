package com.wk.mvvmboilerplate.ui.sociallogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.facebook.login.LoginManager
import com.wk.mvvmboilerplate.R
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_social_login.*
import javax.inject.Inject

/**
 * Activity for user login with facebook or google.
 */

class SocialLoginActivity : AppCompatActivity() {

    private lateinit var socialloginViewModel: SocialLoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_login)

        AndroidInjection.inject(this)
        fblogin.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, arrayListOf("public_profile"))
        }
        socialloginViewModel = ViewModelProviders.of(this)
            .get(SocialLoginViewModel::class.java)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        socialloginViewModel.callbackManager.onActivityResult(requestCode,resultCode, data)
    }
}
