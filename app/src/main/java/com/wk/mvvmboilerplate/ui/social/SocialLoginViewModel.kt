package com.wk.mvvmboilerplate.ui.sociallogin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.facebook.CallbackManager
import com.wk.mvvmboilerplate.ui.login.LoginResult
import javax.inject.Inject
import com.facebook.FacebookException
import com.facebook.FacebookCallback
import com.facebook.login.LoginManager


/**
 * View model class for social login
 */
class SocialLoginViewModel @Inject constructor() : ViewModel() {
    /**
     * livedata to observe login data api
     */
    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    val callbackManager = CallbackManager.Factory.create()
    /**
     * init block to initialize facebook login manager
     */
    init {
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<com.facebook.login.LoginResult> {
                override fun onSuccess(floginResult: com.facebook.login.LoginResult) {
                    _loginResult.value = LoginResult()
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    // App code
                }
            })
    }
    /**
     * function
     */
    fun glogin() {

    }
}
