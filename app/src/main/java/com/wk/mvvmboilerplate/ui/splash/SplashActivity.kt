package com.wk.mvvmboilerplate.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.ui.base.BaseActivity
import com.wk.mvvmboilerplate.ui.dashboard.DashboardActivity
import com.wk.mvvmboilerplate.ui.login.LoginActivity
import com.wk.mvvmboilerplate.ui.sociallogin.SplashViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject
import androidx.core.os.HandlerCompat.postDelayed
import android.os.Handler
import androidx.test.espresso.idling.CountingIdlingResource


/**
 * Activity for splash screen to decide on navigation to login or dashboard.
 */

class SplashActivity : BaseActivity() {

    var idleResource = CountingIdlingResource("splash")

    @Inject
    lateinit var splashViewModelFactory:SplashViewModelFactory
    lateinit var splashViewModel:SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.wk.mvvmboilerplate.R.layout.activity_splash)

        AndroidInjection.inject(this)

        splashViewModel = ViewModelProviders.of(this, splashViewModelFactory)
            .get(SplashViewModel::class.java)

        splashViewModel.navigationDecision.observe(this@SplashActivity, Observer {
                if(it.isUserLoggedIn){
                    val screen = Intent(this@SplashActivity,DashboardActivity::class.java)
                    navTo(screen)
                }else{
                    val screen = Intent(this@SplashActivity,LoginActivity::class.java)
                    navTo(screen)
                }
                idleResource.decrement()
            })
        idleResource.increment()
            Handler().postDelayed( { splashViewModel.checkLogin() }, 3000)

    }

    /**
     * common fun for navigation to specified intent.
     */
    private fun navTo(screen:Intent){
        finish()
        startActivity(screen)
    }
}
