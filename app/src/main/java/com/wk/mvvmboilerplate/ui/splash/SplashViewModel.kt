package com.wk.mvvmboilerplate.ui.splash

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.facebook.CallbackManager
import com.wk.mvvmboilerplate.ui.login.LoginResult
import javax.inject.Inject
import com.facebook.FacebookException
import com.facebook.FacebookCallback
import com.facebook.login.LoginManager
import com.wk.mvvmboilerplate.ui.login.BaseViewModel
import com.wk.mvvmboilerplate.ui.splash.NavigationDecision
import com.wk.mvvmboilerplate.utils.PrefUtils

/**
 * View model class for splash
 */
class SplashViewModel @Inject constructor() : BaseViewModel() {
    /**
     * livedata to observe navigation decision on splash
     */
    private val _navigationDecision = MutableLiveData<NavigationDecision>()
    val navigationDecision:LiveData<NavigationDecision> = _navigationDecision
    /**
     * function to check for user login
     */
    fun checkLogin() {
        var user = prefUtils.getPrefUser()
        Log.i("SplashView","${user?.accessToken}")
        _navigationDecision.value  = if(user!=null && user.status == 1)
            NavigationDecision(true)
        else
            NavigationDecision(false)
    }
}