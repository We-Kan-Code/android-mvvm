package com.wk.mvvmboilerplate.ui.sociallogin

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.wk.mvvmboilerplate.ui.splash.SplashViewModel
import javax.inject.Inject

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class SplashViewModelFactory @Inject constructor(private val splashViewModel: SplashViewModel): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {
            return splashViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
