package com.wk.mvvmboilerplate.ui.updateprofile

import android.app.NotificationManager
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText

import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.ui.base.BaseActivity
import dagger.android.AndroidInjection
import javax.inject.Inject
import com.wk.mvvmboilerplate.utils.Constants
import kotlinx.android.synthetic.main.activity_updateprofile.*
import android.content.Context
import android.os.StrictMode
import androidx.core.app.NotificationCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.wk.mvvmboilerplate.BuildConfig
import com.wk.mvvmboilerplate.utils.AppUtils


/**
 * Activity to update user profile or password.
 */
class UpdateProfileActivity : BaseActivity() {

    lateinit var mNotificationManager:NotificationManager
    lateinit var notification:NotificationCompat.Builder
    lateinit var tsIntent:Intent
    @Inject
    lateinit var updateProfileViewModelFactory: UpdateProfileViewModelFactory
    private lateinit var updateProfileViewModel: UpdateProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        setContentView(com.wk.mvvmboilerplate.R.layout.activity_updateprofile)
        AndroidInjection.inject(this)
        updateProfileViewModel = ViewModelProviders.of(this, updateProfileViewModelFactory)
            .get(UpdateProfileViewModel::class.java)

        //depending on the flag value update fields(true) or change password fields(false) will be visible
        if(intent.getBooleanExtra(Constants.UPDATE_CHANGEPASS,false)){
            email.visibility = View.VISIBLE
            firstname.visibility = View.VISIBLE
            lastname.visibility = View.VISIBLE
            profile_pic.visibility = View.VISIBLE
            updateProfilePic.visibility = View.VISIBLE
            password.visibility = View.GONE
            confpassword.visibility = View.GONE
            updateProfile.visibility = View.VISIBLE
            change_pass.visibility = View.GONE
            updateProfileViewModel.loadUserData()
        }else{
            email.visibility = View.GONE
            firstname.visibility = View.GONE
            lastname.visibility = View.GONE
            profile_pic.visibility = View.GONE
            updateProfilePic.visibility = View.GONE
            password.visibility = View.VISIBLE
            confpassword.visibility = View.VISIBLE
            updateProfile.visibility = View.GONE
            change_pass.visibility = View.VISIBLE
            password.afterTextChanged {
                validateChangePassForm()
            }
            confpassword.apply {
                afterTextChanged {
                    validateChangePassForm()
                }

                setOnEditorActionListener { _, actionId, _ ->
                    when (actionId) {
                        EditorInfo.IME_ACTION_DONE ->
                            updateProfileViewModel.changePassword(password.text.toString())
                    }
                    false
                }

                change_pass.setOnClickListener{
                    loading.visibility = View.VISIBLE
                    updateProfileViewModel.changePassword(password.text.toString())
                }
            }
        }

        updateProfileViewModel.updateProgress.observe(this@UpdateProfileActivity, Observer {
            it ?: return@Observer

            upload_progress_bar.progress = it
        })

        updateProfileViewModel.updateProfileFormState.observe(this@UpdateProfileActivity, Observer {
            val updateProfileFormState = it ?: return@Observer

            // disable login button unless both username / password is valid
            updateProfile.isEnabled = updateProfileFormState.isDataValid

            if (updateProfileFormState.emailError != null) {
                email.error = getString(updateProfileFormState.emailError)
            }
            if (updateProfileFormState.firstNameError != null) {
                firstname.error = getString(updateProfileFormState.firstNameError)
            }
            if (updateProfileFormState.lastNameError != null) {
                lastname.error = getString(updateProfileFormState.lastNameError)
            }
        })
        updateProfileViewModel.changePassswordForm.observe(this@UpdateProfileActivity, Observer {
            val changePassswordFormState  = it ?: return@Observer

            // disable login button unless both username / password is valid
            change_pass.isEnabled = changePassswordFormState.isDataValid

            if (changePassswordFormState.passwordError != null) {
                password.error = getString(changePassswordFormState.passwordError)
            }
            if (changePassswordFormState.cpasswordError != null) {
                confpassword.error = getString(changePassswordFormState.cpasswordError)
            }
        })
//
        updateProfileViewModel.updateProfileResult.observe(this@UpdateProfileActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showToast(loginResult.error)
            }
            if (loginResult.errorCode != null) {
                showToast(loginResult.errorCode)
            }
            if (loginResult.success != null) {
                if(loginResult.success.profileImageUrl!!.isNotEmpty()){
                    var options = RequestOptions()
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .error(R.mipmap.ic_launcher_round)
                    showToast(com.wk.mvvmboilerplate.R.string.profile_pic_update_success)
                    if(notification!=null) {
                        mNotificationManager =
                            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                        notification.setContentTitle(getString(R.string.uploap_completed))
                        notification.setContentText(getString(R.string.upload_progress, 1, 1))
                        notification.setOngoing(false)
                        application.stopService(tsIntent)
                        mNotificationManager.notify(
                            AppUtils.S3_NOTIFICATION_ID,
                            notification.build()
                        )
                    }
                }else {
                    showToast(com.wk.mvvmboilerplate.R.string.update_success)
                }
            }
            upload_progress_bar.visibility = View.GONE
        })

        updateProfileViewModel.profileLoadResult.observe(this@UpdateProfileActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showToast(loginResult.error)
            }
            if (loginResult.errorCode != null) {
                showToast(loginResult.errorCode)
            }
            if (loginResult.success != null) {
                email.setText(loginResult.success.email)
                firstname.setText(loginResult.success.firstName)
                lastname.setText(loginResult.success.lastName)
                    var options = RequestOptions()
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .error(R.mipmap.ic_launcher_round)
            }
            email.afterTextChanged {
                validateForm()
            }
            firstname.afterTextChanged {
                validateForm()
            }
            lastname.apply {
                afterTextChanged {
                    validateForm()
                }
                setOnEditorActionListener { _, actionId, _ ->
                    when (actionId) {
                        EditorInfo.IME_ACTION_DONE ->
                            updateProfileViewModel.update(firstname.text.toString(),lastname.text.toString()
                            )
                    }
                    false
                }

                updateProfile.setOnClickListener{
                    loading.visibility = View.VISIBLE
                    updateProfileViewModel.update(firstname.text.toString(),lastname.text.toString())
                }
            }

        })

        updateProfilePic.setOnClickListener{

        }

    }

    private fun validateForm(){
        updateProfileViewModel.updateDataChanged(
            email.text.toString(),firstname.text.toString(),lastname.text.toString()
        )
    }

    private fun validateChangePassForm(){
        updateProfileViewModel.changePassDataChanged(
            password.text.toString(),confpassword.text.toString()
        )
    }


}


/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
