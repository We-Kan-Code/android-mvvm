package com.wk.mvvmboilerplate.ui.verifyotp

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo

import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.ui.base.BaseActivity
import com.wk.mvvmboilerplate.ui.dashboard.DashboardActivity
import com.wk.mvvmboilerplate.ui.signup.afterTextChanged
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_verify_otp.*
import javax.inject.Inject

/**
* Activity for user otp verification
*/
class VerifyOTPActivity : BaseActivity() {

    @Inject
    lateinit var verifyOTPViewModelFactory:VerifyOTPViewModelFactory
    private lateinit var verifyOTPViewModel:VerifyOTPViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_verify_otp)
        AndroidInjection.inject(this)

        verifyOTPViewModel = ViewModelProviders.of(this, verifyOTPViewModelFactory)
            .get(VerifyOTPViewModel::class.java)

        verifyOTPViewModel.verifyOTPFormState.observe(this@VerifyOTPActivity, Observer {
            val verifyOTPState = it ?: return@Observer

            // disable verifyOTP button unless both username / password is valid
            verify_otp.isEnabled = verifyOTPState.isDataValid

            if (verifyOTPState.usernameError != null) {
                password.error = getString(verifyOTPState.usernameError)
            }
            if (verifyOTPState.passwordError != null) {
                password.error = getString(verifyOTPState.passwordError)
            }
        })

        verifyOTPViewModel.verifyOTPResult.observe(this@VerifyOTPActivity, Observer {
            val verifyOTPResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (verifyOTPResult.error != null) {
                showToast(verifyOTPResult.error)
            }
            if (verifyOTPResult.errorCode != null) {
                showToast(verifyOTPResult.errorCode)
            }
            if (verifyOTPResult.success != null) {
                if (verifyOTPResult.success.status == 1){
                    navToDash(verifyOTPResult.success)
                    finish()
                }

            }
        })
        verifyOTPViewModel.resendOTPResult.observe(this@VerifyOTPActivity, Observer {
            val resendOTPResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (resendOTPResult .error != null) {
                showToast(resendOTPResult.error)
            }
            if (resendOTPResult .errorCode != null) {
                showToast(resendOTPResult.errorCode)
            }
            if (resendOTPResult .success != null) {
                    showToast(resendOTPResult.success)
            }
        })
        password.apply {
            afterTextChanged {
                verifyOTPViewModel.verifyOTPDataChanged(
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        verifyOTPViewModel.verifyOTP(
                            password.text.toString()
                        )
                }
                false
            }

            verify_otp.setOnClickListener {
                loading.visibility = View.VISIBLE
                verifyOTPViewModel.verifyOTP(password.text.toString())
            }
        }

        resend_otp.setOnClickListener{
            loading.visibility = View.VISIBLE
            verifyOTPViewModel.resendOTP()
        }
    }
    /**
     * Navigate to dash board on otp verification success
     */
    private fun navToDash(model: User) {
        val welcome = getString(R.string.welcome)
        val displayName = model.firstName
        showToast("$welcome $displayName")
        startActivity(Intent(this,DashboardActivity::class.java))
    }
}
