package com.wk.mvvmboilerplate.ui.verifyotp

import com.wk.mvvmboilerplate.data.model.room.entity.User

/**
 * OTP verification result : success (user details) or error message.
 */
data class VerifyOTPResult(
    val success: User? = null,
    val errorCode: Int? = null,
    val error: String? = null
)

/**
 * Resend verification result : success (success msg : String) or error message.
 */
data class ResendOTPResult(
    val success: String? = null,
    val errorCode: Int? = null,
    val error: String? = null
)
