package com.wk.mvvmboilerplate.ui.verifyotp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wk.mvvmboilerplate.data.remote.LoginRepository
import com.wk.mvvmboilerplate.data.remote.Result

import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.remote.VerifyOTPRepository
import com.wk.mvvmboilerplate.ui.login.BaseViewModel
import com.wk.mvvmboilerplate.ui.login.LoginFormState
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.convertToHashString
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.isEmailValid
import com.wk.mvvmboilerplate.utils.PrefUtils
import kotlinx.coroutines.*
import javax.inject.Inject

class VerifyOTPViewModel @Inject constructor(private val verifyOTPRepository: VerifyOTPRepository, private val prefUtil: PrefUtils) : BaseViewModel() {

    /**
     * livedata to observe form data change
     */
    private val _verifyOTPForm = MutableLiveData<VerifyOTPFormState>()
    val verifyOTPFormState: LiveData<VerifyOTPFormState> = _verifyOTPForm

    /**
     * livedata to observe verify otp api
     */
    private val _verifyOTPResult = MutableLiveData<VerifyOTPResult>()
    val verifyOTPResult: LiveData<VerifyOTPResult> = _verifyOTPResult

    /**
     * livedata to observe resend otp api
     */
    private val _resendOTPResult = MutableLiveData<ResendOTPResult>()
    val resendOTPResult: LiveData<ResendOTPResult> = _resendOTPResult


    /**
     * function to verify otp on api call
     */
    fun verifyOTP(password: String) {
        // can be launched in a separate asynchronous job
        launch {
            var user = prefUtil.getPrefUser()
            val result = verifyOTPRepository.verifyOTP(user?._id!!, password)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success -> _verifyOTPResult.value =
                        VerifyOTPResult(success = result.data)
                    is Result.Error -> _verifyOTPResult.value = VerifyOTPResult(error = result.msg,errorCode = result.resId)
                    else -> _verifyOTPResult.value = VerifyOTPResult(errorCode = R.string.unknown_error)
                }


            }
        }


    }

    /**
     * function to resend otp through api call
     */
    fun resendOTP() {
        // can be launched in a separate asynchronous job
        launch {
            var user = prefUtil.getPrefUser()
            val result = verifyOTPRepository.resendOTP(user?._id!!)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success -> _resendOTPResult.value =
                        ResendOTPResult(success = "Successfully send")
                    is Result.Error -> _resendOTPResult.value = ResendOTPResult(error = result.msg,errorCode = result.resId)
                    else -> _resendOTPResult.value = ResendOTPResult(errorCode = R.string.unknown_error)
                }


            }
        }


    }
    /**
     * function to validate form changes
     */
    fun verifyOTPDataChanged(password: String) {
        if (password.isEmpty()) {
            _verifyOTPForm.value = VerifyOTPFormState(passwordError = R.string.no_code)
        } else {
            _verifyOTPForm.value = VerifyOTPFormState(isDataValid = true)
        }
    }
}
