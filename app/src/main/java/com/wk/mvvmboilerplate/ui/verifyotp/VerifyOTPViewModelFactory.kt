package com.wk.mvvmboilerplate.ui.verifyotp

import androidx.lifecycle.ViewModel
import com.wk.mvvmboilerplate.ui.login.BaseViewModelFactory
import com.wk.mvvmboilerplate.ui.login.LoginViewModel
import javax.inject.Inject

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class VerifyOTPViewModelFactory @Inject constructor(private val verifyOTPViewModel: VerifyOTPViewModel): BaseViewModelFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(VerifyOTPViewModel::class.java)) {
            return verifyOTPViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
