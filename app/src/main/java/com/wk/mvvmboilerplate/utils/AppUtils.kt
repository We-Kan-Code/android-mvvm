package com.wk.mvvmboilerplate.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Patterns
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import com.wk.mvvmboilerplate.R
import java.security.MessageDigest

class AppUtils{
    companion object{
        /**
         * Password validation
         */
        fun validatePassword(password:String):Boolean{
            return password.matches(Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@\$!%*?&])[A-Za-z\\d@\$!%*?&]{8,}\$"))
        }
        /**
         * Email validation
         */
        fun isEmailValid(email: String): Boolean {
            return if (email.isNotBlank()) {
                Patterns.EMAIL_ADDRESS.matcher(email).matches()
            } else {
                false
            }
        }

        /**
         * Supported algorithms on Android:
         *
         * Algorithm	Supported API Levels
         * MD5          1+
         * SHA-1	    1+
         * SHA-224	    1-8,22+
         * SHA-256	    1+
         * SHA-384	    1+
         * SHA-512	    1+
         */
        fun convertToHashString(algorithm: String, text: String): String {
            val HEX_CHARS = "0123456789abcdef"
            val bytes = MessageDigest
                .getInstance(algorithm)
                .digest(text.toByteArray())
            val result = StringBuilder(bytes.size * 2)

            bytes.forEach {
                val i = it.toInt()
                result.append(HEX_CHARS[i shr 4 and 0x0f])
                result.append(HEX_CHARS[i and 0x0f])
            }
            return result.toString()
        }

        val S3_CHANNEL_ID = "upload_channel"
        val S3_NOTIFICATION_ID = 3000

        fun createAppNotification(context:Context,title:String,content:String):NotificationCompat.Builder{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                createNotificationChannel(context)
            }
            var builder = NotificationCompat.Builder(context, S3_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(content)
            return builder
        }
        lateinit var notificationManager: NotificationManager
        private fun createNotificationChannel(context:Context) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = context.getString(R.string.channel_name)
                val descriptionText = context.getString(R.string.channel_description)
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel(S3_CHANNEL_ID, name, importance).apply {
                    description = descriptionText
                }
                // Register the channel with the system
                notificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }
    }
}

